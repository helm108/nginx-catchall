FROM nginx:alpine
COPY ./redirects.conf /etc/nginx/conf.d/default.conf
CMD nginx -g "daemon off;"
